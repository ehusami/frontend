import { Component, OnInit } from '@angular/core';
import { BookingService } from '../shared/booking.service'
import { Booking, LUTimeSlot, LUTreatment } from '../shared/booking.model';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { Pipe, PipeTransform } from '@angular/core';
import { FilterArrayPipe } from './filter.pip'
@Component({
  selector: 'app-booking-list',
  templateUrl: './booking-list.component.html',
  styleUrls: ['./booking-list.component.css'],
  providers: [BookingService]
})
 @Pipe({name:"filterText"})
export class BookingListComponent implements OnInit {
  public bookingList: Booking[];
  public timeslot: LUTimeSlot[];
  public treatments1 = []
  errorMessage: string;
  activeItem = {};
  constructor(private bookingService: BookingService, private toastr: ToastrService) { }

  ngOnInit() {
    this.bookingService.GetAllBooking();

    // ***************************************GET BOOKING****************************************//
    this.bookingService.getbooking().subscribe(res => { this.bookingList = res, error => this.errorMessage = <any>error });
    // ***************************************GET TIMESLOT ****************************************//
    this.bookingService.gettimeslot().subscribe(res => {
      this.timeslot = res, error => this.errorMessage = <any>error;
    });
    // ****************************************GET TREATMENT ***************************************//
    this.bookingService.gettreatments().subscribe(data => {
      this.treatments1 = data, error => this.errorMessage = <any>error;
    })
  }

  showForEdit(book :Booking) {
    this.activeItem = book;
    this.bookingService.selectedBooking = Object.assign({}, book);;

  }

  showForDetail(book) {
    this.activeItem = book;
  }

  // **************************************** delete function  ***************************************//
  onDelete(id: number) {
    if (confirm('Are you sure to delete this record ?') == true) {
      this.bookingService.DeleteBooking(id)
        .subscribe(x => {
          console.log(id, "iiiiiiid", "book")
          this.bookingService.getbooking().subscribe(res => this.bookingList = res, error => this.errorMessage = <any>error);
          this.toastr.warning("Deleted Successfully", " deleted");
        })
    }
  }

  // **************************************** update function  ***************************************//

  OnUpdate(form: NgForm) {
    console.log(form.value, "foooooooorm")
    this.bookingService.PutBooking(form.value.bookingId, form.value)
      .subscribe(data => {
        this.bookingService.GetAllBooking();
        this.toastr.info('Record Updated Successfully!', 'updated');
      });
  }

}
